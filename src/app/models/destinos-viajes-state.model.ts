import { DestinoViaje } from './destino-viaje.model';
import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClientModule } from '@angular/common/http';

// ESTADO
export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function initializeDestinoViajesState() {
    return {
        items: [],
        loading: false,
        favorito: null
    };
}

// ACCIONES
export enum DestinosViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
    INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}

export class NuevoDestinoAction implements Action {
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) {}
}

export class InitMyDataAction implements Action {
    type = DestinosViajesActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]) {}
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction
    | VoteUpAction | VoteDownAction | InitMyDataAction;

// REDUCERS
export function reducerDestinosViajes(
    state: DestinosViajesState,
    action: DestinosViajesActions
): DestinosViajesState {
    switch (action.type) {
        case DestinosViajesActionTypes.INIT_MY_DATA: {
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
                ...state,
                items: destinos.map((d) => new DestinoViaje(d, ''))
            };
        }
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            console.log('reducerDestinosViajes - NUEVO_DESTINO');
            console.log(state.items);
            console.log(state.favorito);
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino],
                favorito: state.favorito
            };
        }
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
            console.log('reducerDestinosViajes - ELEGIDO_FAVORITO');
            let destinos = state.items.map(x => ({...x}));
            destinos.forEach(x => x.selected = false);
            console.log(destinos);
            let fav: DestinoViaje = <DestinoViaje>destinos.find(d => d.id === (action as ElegidoFavoritoAction).destino.id);
            console.log(fav);
            fav.selected = true;
            console.log(fav);
            return {
                ...state,
                items: <DestinoViaje[]>destinos,
                favorito: fav
            };
        }
        case DestinosViajesActionTypes.VOTE_UP: {
            console.log('reducerDestinosViajes - VOTE_UP');
            let destinos = state.items.map(x => ({...x}));
            let d: DestinoViaje = <DestinoViaje>destinos.find(d => d.id === (action as VoteUpAction).destino.id);
            d.votes++;
            console.log(state);
            return {
                ...state,
                items: <DestinoViaje[]>destinos
            };
        }
        case DestinosViajesActionTypes.VOTE_DOWN: {
            console.log('reducerDestinosViajes - VOTE_DOWN');
            let destinos = state.items.map(x => ({...x}));
            let d: DestinoViaje = <DestinoViaje>destinos.find(d => d.id === (action as VoteDownAction).destino.id);
            d.votes--;
            console.log(state);
            console.log(d);
            return {
                ...state,
                items: <DestinoViaje[]>destinos
            };
        }
    }

    return state;
}

// EFFECTS
@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions) {}
}
