import { createReducer, on, State, Action } from '@ngrx/store';
import * as actions from '../actions/destinos-state.action'
import { DestinoViaje } from '../models/destino-viaje.model';

// STATES
export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export const intialDestinosViajesState: DestinosViajesState = {
    items: [],
    loading: false,
    favorito: null
}

// REDUCER
const _reducerDestinosViajes = createReducer(
    intialDestinosViajesState,
    on(
        actions.nuevoDestinoAction,
        (state, {d}) => ({
            items: [...state.items, d],
            loading: false,
            favorito: null
        })
    ),
    on(
        actions.elegidoFavoritoAction,
        (state, {e}) => {
            let destinos = state.items.map(x => ({...x}));
            destinos.forEach(x => x.id === e.id ? x.selected = true : x.selected = false);
            console.log(destinos);
            const destino = destinos.find(d => d.id === e.id);
            console.log(destino);
            const fDestinos = <DestinoViaje[]> destinos;
            console.log(fDestinos);
            return {
                items: fDestinos,
                loading: false,
                favorito: destino
            }
        }
    )
);

export function reducerDestinosViajes(state: DestinosViajesState, action) {
    return _reducerDestinosViajes(state, action);
}
