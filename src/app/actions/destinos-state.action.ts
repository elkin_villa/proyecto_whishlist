import { createAction, props } from '@ngrx/store';
import { DestinoViaje } from '../models/destino-viaje.model';

export const nuevoDestinoAction = createAction('[Destinos Viajes] Nuevo',
    props<{d: DestinoViaje}>()
);

export const elegidoFavoritoAction = createAction('[Destino Viajes] Favorito',
    props<{e: DestinoViaje}>()
);
